# Copyright 2019 Alexandre Díaz
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


{
    "name": "Sale Contact Type",
    "version": "2.0.1.0.0",
    "category": "Sales",
    "license": "AGPL-3",
    "summary": "Define ordering contact type",
    "depends": ["sale_management"],
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "data": ["views/sale_order.xml"],
    "installable": True,
}
