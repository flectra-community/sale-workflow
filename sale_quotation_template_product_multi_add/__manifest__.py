{
    "name": "Sale Quotation Template Product Multi Add",
    "summary": """
        Feature to add multiple products to quotation template """,
    "author": "Ilyas, Ooops404, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "category": "Sale Management",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "depends": ["sale_management"],
    "data": [
        "security/ir.model.access.csv",
        "wizards/sale_template_add_products_view.xml",
        "views/sale_view.xml",
    ],
}
