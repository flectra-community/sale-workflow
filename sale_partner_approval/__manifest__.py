# Copyright 2022 Open Source Integrators
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Sale Partner Approval",
    "summary": "Control Partners that can be used in Sales Orders",
    "version": "2.0.2.0.0",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "category": "Sales",
    "author": "Open Source Integrators, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "maintainers": ["dreispt"],
    "development_status": "Alpha",
    "depends": [
        "partner_stage",  # oca/product-attribute
        "sale_exception",
    ],
    "data": [
        "data/exception_rule.xml",
        "data/init_sale_ok.xml",
        "views/res_partner_stage.xml",
        "views/res_partner.xml",
    ],
}
