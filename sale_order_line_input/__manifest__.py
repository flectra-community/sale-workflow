# Copyright 2018 Tecnativa - Carlos Dauden
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Sale Order Line Input",
    "summary": "Search, create or modify directly sale order lines",
    "version": "2.0.1.0.0",
    "category": "Sales",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["sale_management"],
    "data": ["views/sale_order_line_view.xml"],
}
