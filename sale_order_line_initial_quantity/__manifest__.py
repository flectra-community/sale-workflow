# Copyright 2021 Akretion
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "Sale Order Line Initial Quantity",
    "summary": "Allows to display the initial quantity when the quantity "
    "has been modified on the command line.",
    "version": "2.0.1.0.1",
    "category": "Sales/Sales",
    "author": "Akretion, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "license": "AGPL-3",
    "depends": ["sale"],
    "data": [
        "views/sale.xml",
    ],
    "installable": True,
}
