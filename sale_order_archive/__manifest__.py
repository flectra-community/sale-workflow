# Copyright 2017-2020 Onestein (<https://www.onestein.eu>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Sale Order Archive",
    "summary": "Archive Sale Orders",
    "author": "Onestein, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "category": "Sales",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "depends": ["sale"],
    "data": ["views/sale_order.xml"],
    "installable": True,
}
