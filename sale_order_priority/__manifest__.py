# Copyright 2018 Simone Rubino - Agile Business Group
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Sale Order Priority",
    "summary": "Define priority on sale orders",
    "version": "2.0.1.0.0",
    "category": "Sale Workflow",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "author": "Agile Business Group, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": ["sale_stock"],
    "data": ["views/sale.xml"],
}
