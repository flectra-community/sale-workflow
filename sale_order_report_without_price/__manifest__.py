# Copyright 2021 Akretion - Florian Mounier
{
    "name": "Sale Order Report Without Price",
    "summary": "Allow you to generate quotation and order reports without price.",
    "version": "2.0.1.0.0",
    "author": "Akretion, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "category": "Sales",
    "depends": ["sale"],
    "data": [
        "report/sale_report.xml",
        "report/sale_report_templates.xml",
    ],
    "license": "LGPL-3",
}
