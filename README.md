# Flectra Community / sale-workflow

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[sale_order_invoicing_finished_task](sale_order_invoicing_finished_task/) | 2.0.1.0.0| Control invoice order lines if their related task has been set to invoiceable
[sale_order_line_date](sale_order_line_date/) | 2.0.1.1.0| Adds a commitment date to each sale order line.
[sale_order_line_menu](sale_order_line_menu/) | 2.0.1.0.0| Adds a Sale Order Lines Menu
[sale_product_set_sale_by_packaging](sale_product_set_sale_by_packaging/) | 2.0.1.0.1|     Glue module between `sale_by_packaging` and `sale_product_set_packaging_qty`.    
[sale_order_line_initial_quantity](sale_order_line_initial_quantity/) | 2.0.1.0.1| Allows to display the initial quantity when the quantity has been modified on the command line.
[sales_team_security](sales_team_security/) | 2.0.2.0.0| New group for seeing only sales channel's documents
[sale_order_report_without_price](sale_order_report_without_price/) | 2.0.1.0.0| Allow you to generate quotation and order reports without price.
[sale_restricted_qty](sale_restricted_qty/) | 2.0.1.0.1| Sale order min quantity
[sale_pricelist_from_commitment_date](sale_pricelist_from_commitment_date/) | 2.0.1.0.2| Use sale order commitment date to compute line price from pricelist
[sale_advance_payment](sale_advance_payment/) | 2.0.1.1.1| Allow to add advance payments on sales and then use them on invoices
[sale_global_discount](sale_global_discount/) | 2.0.1.0.0| Sale Global Discount
[sale_default_uom](sale_default_uom/) | 2.0.1.1.0|  Set default Unit of Measure value of a product in sales order lines.
[sale_contact_type](sale_contact_type/) | 2.0.1.0.0| Define ordering contact type
[sale_company_currency](sale_company_currency/) | 2.0.1.0.1| Company Currency in Sale Orders
[sale_product_seasonality](sale_product_seasonality/) | 2.0.1.1.0| Integrates rules for products' seasonal availability with sales
[sale_isolated_quotation](sale_isolated_quotation/) | 2.0.2.1.0| Sale Isolated Quotation
[sale_order_product_assortment](sale_order_product_assortment/) | 2.0.1.0.0| Module that allows to use the assortments on sale orders
[sale_product_brand_exception](sale_product_brand_exception/) | 2.0.1.0.0| Define rules for sale order and brands
[sale_invoice_plan](sale_invoice_plan/) | 2.0.1.0.3| Add to sales order, ability to manage future invoice plan
[sale_procurement_amendment](sale_procurement_amendment/) | 2.0.1.0.0|         Allow to reflect confirmed sale lines quantity amendments        to procurements
[pricelist_cache](pricelist_cache/) | 2.0.1.0.1|         Provide a new model to cache price lists and update it,        to make it easier to retrieve them.    
[pricelist_cache_rest](pricelist_cache_rest/) | 2.0.1.0.0| Provides an endpoint to get product prices for a given customer
[sale_product_multi_add](sale_product_multi_add/) | 2.0.1.0.1|         Sale Product Multi Add 
[sale_order_line_note](sale_order_line_note/) | 2.0.1.0.0| Note on sale order line
[sale_product_rating_verified](sale_product_rating_verified/) | 2.0.1.0.1| Verify if a user has previously bought a product
[sale_invoice_blocking](sale_invoice_blocking/) | 2.0.1.0.0| Allow you to block the creation of invoices from a sale order.
[sale_order_disable_user_autosubscribe](sale_order_disable_user_autosubscribe/) | 2.0.1.0.0| Remove the salesperson from autosubscribed sale followers
[sale_order_lot_selection](sale_order_lot_selection/) | 2.0.1.1.0| Sale Order Lot Selection
[sale_cancel_confirm](sale_cancel_confirm/) | 2.0.1.0.0| Sales Cancel Confirm
[sales_team_security_crm](sales_team_security_crm/) | 2.0.3.0.0| Integrates sales_team_security with crm
[sale_order_mass_action](sale_order_mass_action/) | 2.0.1.0.0|     Allows to easy mass operations on sale orders.
[product_form_sale_link](product_form_sale_link/) | 2.0.1.0.2|         Adds a button on product forms to access Sale Lines
[sales_team_security_sale](sales_team_security_sale/) | 2.0.3.0.0| Integrates sales_team_security with sale
[portal_sale_personal_data_only](portal_sale_personal_data_only/) | 2.0.1.0.0| Portal Sale Personal Data Only
[sale_automatic_workflow_payment_mode](sale_automatic_workflow_payment_mode/) | 2.0.1.0.1| Sale Automatic Workflow - Payment Mode
[sale_order_type](sale_order_type/) | 2.0.3.0.0| Sale Order Type
[sale_order_priority](sale_order_priority/) | 2.0.1.0.0| Define priority on sale orders
[sale_order_note_template](sale_order_note_template/) | 2.0.1.0.0| Add sale orders terms and conditions template that can be used to quickly fullfill sale order terms and conditions
[sale_mrp_bom](sale_mrp_bom/) | 2.0.1.0.0| Allows define a BOM in the sales lines.
[sale_amount_payment_link](sale_amount_payment_link/) | 2.0.1.0.1| Reduce Amount to be paid while Payment Link is generated on Sale Order, depending on done Transactions.
[sale_commercial_partner](sale_commercial_partner/) | 2.0.1.0.1| Add stored related field 'Commercial Entity' on sale orders
[sale_transaction_form_link](sale_transaction_form_link/) | 2.0.1.0.0|         Allows to display a link to payment transactions on Sale Order form view.
[sale_order_line_chained_move](sale_order_line_chained_move/) | 2.0.1.0.1|         This module adds a field on sale order line to get all related move lines
[sale_partner_incoterm](sale_partner_incoterm/) | 2.0.1.1.0| Set the customer preferred incoterm on each sales order
[sale_quotation_number](sale_quotation_number/) | 2.0.2.0.0| Different sequence for sale quotations
[sale_order_invoice_amount](sale_order_invoice_amount/) | 2.0.1.0.0| Display the invoiced and uninvoiced total in the sale order
[sale_delivery_state](sale_delivery_state/) | 2.0.1.1.0| Show the delivery state on the sale order
[sale_blanket_order](sale_blanket_order/) | 2.0.1.0.2| Blanket Orders
[sale_tier_validation](sale_tier_validation/) | 2.0.1.0.0| Extends the functionality of Sale Orders to support a tier validation process.
[sale_commitment_date_mandatory](sale_commitment_date_mandatory/) | 2.0.1.1.0| Set commitment data mandatory and don't allowto add lines unless this field is filled
[sale_order_qty_change_no_recompute](sale_order_qty_change_no_recompute/) | 2.0.1.0.2| Prevent recompute if only quantity has changed in sale order line
[sale_order_line_discount_validation](sale_order_line_discount_validation/) | 2.0.2.0.0| Review discounts before sales order are printed, sent or confirmed
[sale_partner_version](sale_partner_version/) | 2.0.2.0.0| Sale Partner Version
[sale_delivery_split_date](sale_delivery_split_date/) | 2.0.1.0.0| Sale Deliveries split by date
[sale_product_set](sale_product_set/) | 2.0.1.5.4| Sale product set
[sale_order_line_packaging_qty](sale_order_line_packaging_qty/) | 2.0.1.0.1| Define quantities according to product packaging on sale order lines
[sale_quotation_template_product_multi_add](sale_quotation_template_product_multi_add/) | 2.0.1.0.1|         Feature to add multiple products to quotation template 
[sale_order_general_discount_triple](sale_order_general_discount_triple/) | 2.0.1.0.1| General discount per sale order with triple
[sale_automatic_workflow](sale_automatic_workflow/) | 2.0.1.3.1| Sale Automatic Workflow
[sale_order_lot_generator](sale_order_lot_generator/) | 2.0.1.0.1| Sale Order Lot Generator
[sale_stock_picking_note](sale_stock_picking_note/) | 2.0.1.0.0| Add picking note in sale and purchase order
[sale_shipping_info_helper](sale_shipping_info_helper/) | 2.0.1.0.0| Add shipping amounts on sale order
[sale_discount_display_amount](sale_discount_display_amount/) | 2.0.1.1.0|         This addon intends to display the amount of the discount computed on        sale_order_line and sale_order level
[sale_product_category_menu](sale_product_category_menu/) | 2.0.1.0.1| Shows 'Product Categories' menu item in Sales
[sale_order_carrier_auto_assign](sale_order_carrier_auto_assign/) | 2.0.1.0.1| Auto assign delivery carrier on sale order confirmation
[sale_order_warn_message](sale_order_warn_message/) | 2.0.1.1.0|         Add a popup warning on sale to ensure warning is populated
[sale_rental](sale_rental/) | 2.0.1.0.0| Manage Rental of Products
[sale_partner_approval](sale_partner_approval/) | 2.0.2.0.0| Control Partners that can be used in Sales Orders
[sale_order_archive](sale_order_archive/) | 2.0.1.0.0| Archive Sale Orders
[sale_order_general_discount](sale_order_general_discount/) | 2.0.1.0.1| General discount per sale order
[sale_exception](sale_exception/) | 2.0.1.1.0| Custom exceptions on sale order
[product_supplierinfo_for_customer_sale](product_supplierinfo_for_customer_sale/) | 2.0.1.0.1| Loads in every sale order line the customer code defined in the product
[sale_automatic_workflow_delivery_state](sale_automatic_workflow_delivery_state/) | 2.0.1.0.0| Glue module for sale_automatic_workflow and sale_delivery_state
[sale_start_end_dates](sale_start_end_dates/) | 2.0.1.0.0| Adds start date and end date on sale order lines
[sale_delivery_date](sale_delivery_date/) | 2.0.1.0.2| Postpones delivery dates based on customer preferences, and/or warehouse configuration.
[sale_order_line_description](sale_order_line_description/) | 2.0.1.0.0| Sale order line description
[sale_last_price_info](sale_last_price_info/) | 2.0.1.0.1| Product Last Price Info - Sale
[sale_procurement_group_by_line](sale_procurement_group_by_line/) | 2.0.1.0.1| Base module for multiple procurement group by Sale order
[sale_wishlist](sale_wishlist/) | 2.0.1.0.2|         Handle sale wishlist for partners
[sale_by_packaging](sale_by_packaging/) | 2.0.2.0.0| Manage sale of packaging
[sale_order_revision](sale_order_revision/) | 2.0.1.1.0| Keep track of revised quotations
[sale_force_invoiced](sale_force_invoiced/) | 2.0.1.1.1| Allows to force the invoice status of the sales order to Invoiced
[sale_stock_picking_blocking](sale_stock_picking_blocking/) | 2.0.1.1.0| Allow you to block the creation of deliveries from a sale order.
[sale_mail_autosubscribe](sale_mail_autosubscribe/) | 2.0.1.0.0| Automatically subscribe partners to their company's sale orders
[sale_triple_discount](sale_triple_discount/) | 2.0.1.0.0| Manage triple discount on sale order lines
[sale_order_line_input](sale_order_line_input/) | 2.0.1.0.0| Search, create or modify directly sale order lines
[sale_order_line_delivery_state](sale_order_line_delivery_state/) | 2.0.1.0.0| Show the delivery state on the sale order line
[sale_order_line_price_history](sale_order_line_price_history/) | 2.0.1.0.0| Sale order line price history
[sale_stock_delivery_address](sale_stock_delivery_address/) | 2.0.1.0.2| Sale Stock Delivery Address
[sale_validity](sale_validity/) | 2.0.1.0.2| Set a default validity delay on quotations
[sale_automatic_workflow_job](sale_automatic_workflow_job/) | 2.0.1.0.0| Execute sale automatic workflows in queue jobs
[sale_product_set_packaging_qty](sale_product_set_packaging_qty/) | 2.0.1.1.0| Manage packaging and quantities on product set lines


