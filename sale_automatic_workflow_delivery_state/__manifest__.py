{
    "name": "Sale Automatic Workflow Delivery State",
    "summary": "Glue module for sale_automatic_workflow and sale_delivery_state",
    "version": "2.0.1.0.0",
    "category": "Sales Management",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "author": "Akretion, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": [
        "sale_automatic_workflow",
        "sale_delivery_state",
    ],
    "auto_install": True,
}
