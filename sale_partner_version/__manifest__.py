# Copyright 2018 Akretion - Benoît Guillot
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Sale Partner Version",
    "version": "2.0.2.0.0",
    "author": "Akretion, " "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "category": "Sale",
    "license": "AGPL-3",
    "installable": True,
    "depends": ["sale", "partner_address_version"],
    "data": [],
}
