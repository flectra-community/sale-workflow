# Copyright 2020 ForgeFlow S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Sale Order Warn Message",
    "summary": """
        Add a popup warning on sale to ensure warning is populated""",
    "version": "2.0.1.1.0",
    "license": "AGPL-3",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/sale-workflow",
    "depends": ["sale"],
    "data": ["views/sale_order.xml"],
}
